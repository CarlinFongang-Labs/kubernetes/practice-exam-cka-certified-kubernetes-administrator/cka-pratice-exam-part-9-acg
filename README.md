# 9. CKA Pratice Exam Part 9 ACG : Pod multicontainer & Sidecar

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectif

1. Créer un pod multi-conteneurs

2. Créez un pod qui utilise un side-car pour exposer le fichier journal du conteneur principal à « stdout »


# Contexte

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées, ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le véritable examen CKA. Bonne chance!

Cette question utilise le acgk8scluster. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud, c'est-à-dire ssh acgk8s-worker1.

Remarque : Vous ne pouvez pas vous connecter à un autre nœud, ni l'utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de exitrevenir au nœud racine avant de continuer.

Vous pouvez exécuter le script de vérification situé à /home/cloud_user/verify.shtout moment pour vérifier votre travail !

>![Alt text](img/image.png)

# Application



## Étape 1 : Connexion au Serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

   ```sh
   ssh cloud_user@<PUBLIC_IP_ADDRESS>
   ```

## Étape 2 : Créer un Pod Multi-Conteneurs

1. Passez au contexte `acgk8s` :

   ```sh
   kubectl config use-context acgk8s
   ```

2. Créez un fichier YAML nommé `multi.yml` :

   ```sh
   vim multi.yml
   ```

3. Dans le fichier, collez ce qui suit :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: multi
  namespace: baz
spec:
  containers:
  - name: nginx
    image: nginx
    ports:
    - containerPort: 80
  - name: redis
    image: redis
```

Ref doc : 

https://kubernetes.io/docs/concepts/workloads/pods/#using-pods

4. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

5. Créez le pod multi-conteneurs :

```sh
kubectl create -f multi.yml
```

6. Vérifiez l'état du pod :

```sh
kubectl get pods -n baz
```


7. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

>![Alt text](img/image-1.png)

## Étape 3 : Créer un Pod qui Utilise un Sidecar pour Exposer le Fichier Journal du Conteneur Principal à Stdout

1. Créez un fichier YAML nommé `logging-sidecar.yml` :

   ```sh
   vim logging-sidecar.yml
   ```

2. Dans le fichier, collez ce qui suit :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: logging-sidecar
  namespace: baz
spec:
  containers:
  - name: busybox1
    image: busybox
    command: ['sh', '-c', 'while true; do echo Logging data > /output/output.log; sleep 5; done']
    volumeMounts:
      - name: sharevol
        mountPath: /output
  - name: sidecar
    image: busybox
    command: ['sh', '-c', 'tail -F /input/output.log']
    volumeMounts:
      - name: sharevol
        mountPath: /input
  volumes:
    - name: sharevol
      emptyDir: {}
```

Ref doc: 

https://kubernetes.io/docs/concepts/workloads/pods/sidecar-containers/#sidecar-example

3. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

4. Créez le pod `logging-sidecar` :

```sh
kubectl create -f logging-sidecar.yml
```

5. Vérifiez l'état du pod :

```sh
kubectl get pods -n baz
```

>![Alt text](img/image-2.png)

6. Vérifiez les journaux du conteneur `sidecar` dans le pod `logging-sidecar` :

```sh
kubectl logs logging-sidecar -n baz -c sidecar
```

>![Alt text](img/image-3.png)

7. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

>![Alt text](img/image-4.png)

En suivant ces étapes, vous aurez créé un pod multi-conteneurs et un pod avec un sidecar pour exposer les journaux du conteneur principal à Stdout.